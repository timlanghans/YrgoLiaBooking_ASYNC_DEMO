package db;

import static tools.JSON_Tools.createJSONerrorMessage;
import static tools.LOG_Tools.log;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;

import exceptionhandling.ApiException;

@Stateless
public class EndpointExceptionHandler {

	private final static Logger logger = Logger.getLogger(EndpointExceptionHandler.class.getName());

	/**
	 * Error-messages to be returned to the client in case of different
	 * Exceptions.
	 */
	public static final String NOT_FOUND = "Not Found";
	public static final String ENTITY_EXISTS = "An entity for the desired values exists already.";
	public static final String UNEXPECTED_ERROR = "An unexpected error has occured. Try again later.";
	public static final String CONSTRAINT_VIOLATION = "User parameters contain values that are not allowed.";
	public static final String JSON_MESSAGE_VALUE = "message";
	public static final String ENTITY_NOT_FOUND = "A required entity was not found. Please check your arguments. ";

	/**
	 * Method handling all Exception-types that may occur in the Rest-endpoints.
	 * Returns a <code>javax.ws.rs.core.Response</code> response with a matching
	 * http status code and an JSON-errormessage as the response body.
	 * Exceptiontypes that are not expected return a status 500 response.
	 * 
	 * @param e
	 *            - the {@link java.lang.Exception} that occured
	 * @return {@javax.ws.rs.core.Response} matching the Exception type
	 * 
	 */
	public static Response handleException(Exception e) {

		if (e instanceof NoResultException) {
			log(logger, e);
			return Response.status(404).entity(createJSONerrorMessage(NOT_FOUND)).build();
		} else if (e instanceof EntityExistsException) {
			log(logger, e);
			return Response.status(400).entity(createJSONerrorMessage(ENTITY_EXISTS)).build();
		} else if (e instanceof EntityNotFoundException) {
			log(logger, e);
			return Response.status(400).entity(createJSONerrorMessage(ENTITY_NOT_FOUND)).build();
		} else if (e instanceof ConstraintViolationException) {
			log(logger, e);
			return Response.status(400).entity(createJSONerrorMessage(CONSTRAINT_VIOLATION)).build();
		} else {
			log(logger, e);
			return Response.status(500).entity(createJSONerrorMessage(UNEXPECTED_ERROR)).build();
		}
	}
}
