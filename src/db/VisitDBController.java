package db;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import json.Visit_IdOnly_json;
import model.Liaplace;
import model.Student;
import model.Teacher;
import model.Visit;

@Stateless
@Transactional
public class VisitDBController  {

	@Inject
	private EntityManager em;

	private Logger logger = Logger.getLogger(VisitDBController.class.getName());

	public Visit findOneById(Integer id) throws Exception {
		Visit v = em.find(Visit.class, id);
		if (v == null) {
			throw new EntityNotFoundException("No entity with id " + id);
		} else {
			return v;
		}
	}

	
	public Integer fillNewVisitFromJsonInput(Visit_IdOnly_json input) throws Exception {
			Liaplace place = em.find(Liaplace.class, input.getLiaplace_id());
			Student student = em.find(Student.class, new Integer(input.getStudent_id()));
			Teacher teacher = em.find(Teacher.class, input.getTeacher_id());
			Visit visit = new Visit(student, place, teacher, input.getVisittime());
			em.persist(visit);
			logger.log(Level.SEVERE, "Created new Visit " + visit.getId());
			return visit.getId();
	}

	// returns same visit, assumes that visit is correct (all fields set)
	public void updateVisitFromJsonInput(Integer id, Visit_IdOnly_json input) throws Exception {

		Visit visit = em.find(Visit.class, id);
		if (visit == null ) throw new EntityNotFoundException("Visit " + id + "cannot be found!");
		
		if (!visit.getLiaplace().getId().equals(input.getLiaplace_id())) {
			visit.setLiaplace(em.find(Liaplace.class, input.getLiaplace_id()));
		}

		if (!visit.getStudent().getId().equals(input.getStudent_id())) {
			visit.setStudent(em.find(Student.class, input.getStudent_id()));
		}

		if (!visit.getTeacher().getId().equals(input.getTeacher_id())) {
			visit.setTeacher(em.find(Teacher.class, input.getTeacher_id()));
		}

		if (!visit.getVisittime().equals(input.getVisittime())) {
			visit.setVisittime(input.getVisittime());
		}

		logger.log(Level.SEVERE, visit.toString());
		// em.find returns null if entity does not exist!
		if (visit.getLiaplace() == null || visit.getStudent() == null || visit.getTeacher() == null) {
			throw new IllegalArgumentException("Problems with your update values!");
		}
		em.persist(visit);
	}

	
	public List<Visit> findListWithNamedQuery(String queryStr, String... params) {
		return findListWithNamedQuery(queryStr, 0, Integer.MAX_VALUE, params);
	}

	
	public List<Visit> findListWithNamedQuery(String queryStr, int startPos, int maxResult, String... params) {
		TypedQuery<Visit> query = em.createNamedQuery(queryStr, Visit.class);
		query.setFirstResult(startPos);
		query.setMaxResults(maxResult);

		for (int i = 1; i <= params.length; i++) {
			query.setParameter(i, params[i]);
		}
		List<Visit> visits = query.getResultList();
		return visits;
	}

	public Visit findOneWithNamedQuery(String queryStr, String... params) {
		TypedQuery<Visit> query = em.createNamedQuery(queryStr, Visit.class);

		for (int i = 1; i <= params.length; i++) {
			query.setParameter(i, params[i]);
		}
		Visit visits = query.getSingleResult();
		return visits;
	}

	
	public void removeVisit(Integer id) throws Exception {
			Visit visit = em.find(Visit.class, id);
			
			if (visit == null) throw new EntityNotFoundException();
			em.remove(visit);
	}

	
	public boolean containsEntity(Visit visit) throws Exception {
		return em.contains(visit);
	}
}
