package main;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import javax.ws.rs.ext.Provider;

import model.Task;

@Startup
@Singleton
@Provider
@Transactional
public class WorkQueue {
	
	@Inject 
	private TaskDatabaseController dbcontroller;

	private final AtomicInteger id = new AtomicInteger(1000);
	private Map<Integer, CompletableFuture<Task>> mapping = new HashMap<>();

	public synchronized void putTask(Integer id, CompletableFuture<Task> task) {
		mapping.put(id, task);
	}

	public CompletableFuture<Task> getTask(Integer id) {
		CompletableFuture<Task> cfuture = mapping.get(id);

		if (cfuture == null)
			throw new NullPointerException("Mapping not found");
		return cfuture;
	}

	
	public synchronized boolean deleteTask(Integer id, CompletableFuture<String> task) {
		return mapping.remove(id, task);
	}

	
	
	public synchronized Integer submitTask(Supplier<String> task_in) throws Exception {

		CompletableFuture<Task> f_out = new CompletableFuture();
		CompletableFuture<String> future = CompletableFuture.supplyAsync(task_in);

		f_out = future.thenApplyAsync(result -> {
			try {
				final Task task = dbcontroller.persistTask(result);
				return task;
			} catch (Exception e) {
				throw new RuntimeException("Submit task cannot proceed!");
			}
		});

		f_out.thenRun(() -> System.out.println("CALCULATION RETURNED!"));
		mapping.put(id.incrementAndGet(), f_out);
		return id.intValue();
	}

}
