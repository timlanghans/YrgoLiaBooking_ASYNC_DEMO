INSERT INTO  Teacher(name) VALUES ('TEACHER1');
INSERT INTO  Klass(name) VALUES ('CLASS1' );
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (1,1);
INSERT INTO  Teacher(name ) VALUES ('TEACHER2');
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (2,1);
INSERT INTO  Teacher(name) VALUES ('TEACHER3');
INSERT INTO  Klass(name) VALUES ('CLASS2' );
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (3,2);
INSERT INTO  LiaPeriod(name,startDate,endDate,klass_fk) VALUES('LIAPERIOD1','2001-01-01', '2001-03-01' , 1 );
INSERT INTO  Student(name) VALUES ('AUTHSTUDENT1');
INSERT INTO  Student(name) VALUES ('STUDENT2');
INSERT INTO  Student(name) VALUES ('STUDENT3');
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (1,2);
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (2,1);
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (3,1);

INSERT INTO  Company(name) VALUES ('COMPANY1');
INSERT INTO  Company(name) VALUES ('COMPANY2');
INSERT INTO  LiaPlace(name, company_fk) VALUES ('LIAPROJECT1' , 1);
INSERT INTO  LiaPlace(name, company_fk) VALUES ('LIAPROJECT2' , 1);
INSERT INTO  LiaPlace(name, company_fk) VALUES ('LIAPROJECT3' , 2);

INSERT INTO  Visit(liaplace_fk,teacher_fk,student_fk,visitTime) VALUES(1,1,1,'2001-01-01 13:03');
INSERT INTO  Visit(liaplace_fk,teacher_fk,student_fk,visitTime) VALUES(1,2,2,'2002-02-02 14:04');
INSERT INTO  Visit(liaplace_fk,teacher_fk,student_fk,visitTime) VALUES(3,3,3,'2003-03-03 15:05');
