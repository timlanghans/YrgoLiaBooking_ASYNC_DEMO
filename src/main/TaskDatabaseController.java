package main;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import model.Task;

@Stateless
public class TaskDatabaseController {

	@Inject
	EntityManager em;
	
	public Task persistTask(String input){
		final Task task = new Task();
		task.setString(input);
		em.persist(task);
		System.out.println("PERSISTED task " + task.getId());
		return task;
	}
	
	
	
}
