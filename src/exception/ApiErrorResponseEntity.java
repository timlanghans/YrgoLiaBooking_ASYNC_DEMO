package exception;

import javax.ws.rs.core.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApiErrorResponseEntity{
	
	private Response.Status status;
	private String userMessage;
	private String errorMessage;
	
	public Response.Status getStatus() {
		return status;
	}
	public void setStatus(Response.Status status) {
		this.status = status;
	}
	public String getUserMessage() {
		return userMessage;
	}
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	

}
