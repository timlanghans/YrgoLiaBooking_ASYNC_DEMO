package exception;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.adapters.XmlAdapter;
 
public class ResponseStatusAdapter extends XmlAdapter<String, Response.Status> {
 
    @Override
    public String marshal(Response.Status status) throws Exception {
        return status.name();
    }
 
    @Override
    public Response.Status unmarshal(String statusAsString) throws Exception {
        return Response.Status.valueOf(statusAsString);
    }
 
}