package json;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import model.Visit;

public class Visit_json {
	
	private Integer id;
	private Student_json student;
	private Liaplace_json liaplace;
	private Teacher_json teacher;
	// TODO Move pattern to Global settings, CAUTION not displayed as 24:00 but 12:00
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
	private Date visittime;
	
	public Visit_json(){}
	
	public Visit_json(Visit v){
		this.id = v.getId();
		this.visittime = v.getVisittime();
		this.student = new Student_json(v.getStudent());
		this.liaplace = new Liaplace_json(v.getLiaplace());
		this.teacher = new Teacher_json(v.getTeacher());	
	}

	public void setInteger(Integer id){
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public Date getVisittime() {
		return visittime;
	}

	public void setVisittime(Date visittime) {
		this.visittime = visittime;
	}

	public Student_json getStudent() {
		return student;
	}

	public void setStudent(Student_json student) {
		this.student = student;
	}

	public Liaplace_json getLiaplace() {
		return liaplace;
	}

	public void setLiaplace(Liaplace_json liaplace) {
		this.liaplace = liaplace;
	}

	public Teacher_json getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher_json teacher) {
		this.teacher = teacher;
	}
	
	@Override
	public String toString(){
		return "[ id: "+ this.id+ ", visittime:" + visittime.toString() + ", student_id:" + this.getStudent().getId()
				+ ", teacher_id:" + this.getTeacher().getId() + ", liaplace_id: "+ this.getLiaplace().getId()
				+ " ]";
	}
	
}
