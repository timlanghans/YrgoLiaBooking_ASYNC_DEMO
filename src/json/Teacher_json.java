package json;

import model.Teacher;

public class Teacher_json {
	private Integer id;
	private String name;

	public Teacher_json(){}
	
	public Teacher_json(Teacher t){
		this.id = t.getId();
		this.name = t.getName();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
