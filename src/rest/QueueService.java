package rest;

import java.util.concurrent.CompletableFuture;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import main.WorkQueue;
import model.Task;

@Path("/queue")
@Stateless
@Transactional
public class QueueService {

	@Inject
	private WorkQueue queue;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getQueuedTask(@PathParam("id") Integer id, @Context UriInfo uriinfo) throws Exception{
		CompletableFuture<Task> qt = queue.getTask(id);
		Task result;
		
		if (qt.isDone() && (result = qt.get()) != null){
			Integer taskId = result.getId();
			String path = "/tasks/" + taskId;
			return Response.seeOther(uriinfo.getBaseUriBuilder().path(path).build()).build();
		} else{
			return Response.ok(createJsonStatusMessage()).build();
		}
	}
	
	
	private static String createJsonStatusMessage(){
		 JsonObject value = Json.createObjectBuilder()
				 .add("message", "Not finished, come again later!").build();
		 return value.toString();
	}
}
