package rest;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.CompletionCallback;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import db.VisitDBController;
import exceptionhandling.ExceptionInterceptor;
import json.Visit_IdOnly_json;
import json.Visit_json;
import model.Visit;
import tools.JSON_Tools;

/**
 * RESTful service end point handling requests regarding LiaVisits
 *
 * @author Tim Langhans, BookLiaVisit group
 */
@Path("/visits")
@Transactional
@Interceptors(ExceptionInterceptor.class)
public class VisitService {

@Inject
@Default
Logger logger;

	
	@Inject
	private VisitDBController dbcontroller;

	@Resource
	ManagedExecutorService exec;
	
	/**
	 * Retrieves all LiaVisits.
	 *typee
	 * @return A status 200 HTTP-response containing a JSON representation of a
	 *         list of Visit-objects in its body.
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllVisits()  throws Exception{
			List<Visit> visits = dbcontroller.findListWithNamedQuery( Visit.FIND_ALL);
			return Response.ok()
					.entity(JSON_Tools.createJsonDtoOfType(visits, Visit_json.class))
					.build();
	}
	
	
	/**
	 * Testing a async endpoint solution with AsyncResponse and Completion-
	 * Callback class
	 * 
	 * JUST FOR DEMO!
	 * 
	 * @param asyncresponse
	 * @throws Exception
	 */
	@GET
	@Path("/async")
	@Produces({ MediaType.TEXT_PLAIN})
	public void getSomeAsync(@Suspended AsyncResponse asyncresponse)  throws Exception{
		asyncresponse.register(new CompletionCallback(){
			@Override
			public void onComplete(Throwable throwable) {
				logger.log(Level.SEVERE, "I GOT A CALLBACK");
			}
		});
		
		Callable<String> answer = new Callable<String>() {
			@Override
			public String call() throws Exception {
				Thread.sleep(3000);
				return "THE ANSWER";
			}
		};
		Future<String> entity_future = exec.submit(answer);
		try{
			String result = entity_future.get();
				logger.log(Level.SEVERE, "I HAVE SLEPT AND RETURN NOW!");
				asyncresponse.resume(Response.ok("THANKS FOR WAITING! Here is the result: " + result).build());
			} catch (Exception e){
				asyncresponse.cancel();
			}
	}
	
	
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllVisits(@PathParam("id") Integer id) throws Exception {
			Visit visit = dbcontroller.findOneById(id);
			return Response.ok()
					.entity(JSON_Tools.createJsonDtoOfType(visit, Visit_json.class))
					.build();
	}
	
	

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addNewVisit( @Valid Visit_IdOnly_json newVisit) throws Exception{
				dbcontroller.fillNewVisitFromJsonInput(newVisit);
				return Response.noContent().build();
	}

	/**
	 *
	 * @param visit_new
	 * @return
	 */
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateVisit(@PathParam("id") Integer id, Visit_IdOnly_json update) throws Exception {
			dbcontroller.updateVisitFromJsonInput(id, update);
			return Response.noContent().build();
	}

	@DELETE
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteVisit(@PathParam("id") Integer id) throws Exception{
			dbcontroller.removeVisit(id);
			return Response.noContent().build();
	}

	/**
	 * Retrieves all LiaVisits for the requested Student-id.
	 *
	 * @param id
	 *            Student-id
	 * @return A status 200 HTTP-response containing a JSON representation of a
	 *         list of Visit-objects in its body.
	 */
	@GET
	@Path("/student/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVisitsByStudentId(@PathParam("id") Integer id) throws Exception{
			List<Visit> visits = dbcontroller.findListWithNamedQuery(Visit.FIND_BY_STUDENT_ID, id.toString());
			return Response.ok().entity(JSON_Tools.createJsonDtoOfType(visits, Visit_json.class)).build();
	}

	/**
	 * Retrieves all LiaVisits for the requested Teacher-id.
	 *
	 * @param id
	 *            Teacher-id
	 * @return A status 200 HTTP-response containing a JSON representation of a
	 *         list of Visit-objects in its body.
	 */
	@GET
	@Path("/teacher/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVisitsByTeacherId(@PathParam("id") Integer id) throws Exception{
			List<Visit> visits = dbcontroller.findListWithNamedQuery(Visit.FIND_BY_TEACHER_ID, id.toString());
			return Response.ok().entity(JSON_Tools.createJsonDtoOfType(visits, Visit_json.class)).build();
	}

	/**
	 * Retrieves all LiaVisits for the requested LiaPlace-id.
	 *
	 * @param id
	 *            LiaPlace-id
	 * @return A status 200 HTTP-response containing a JSON representation of a
	 *         list of Visit-objects in its body.
	 */
	@GET
	@Path("/liaplace/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVisitsByLiaplaceId(@PathParam("id") Integer id) throws Exception{
			List<Visit> visits = dbcontroller.findListWithNamedQuery(Visit.FIND_BY_PLACE_ID, id.toString());
			return Response.ok().entity(JSON_Tools.createJsonDtoOfType(visits, Visit_json.class)).build();
	}
}