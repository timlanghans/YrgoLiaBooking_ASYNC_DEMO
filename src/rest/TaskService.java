package rest;

import java.util.List;
import java.util.function.Supplier;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import main.WorkQueue;
import model.Task;


@Path("/tasks")
@Stateless
@Transactional
@Produces(MediaType.APPLICATION_JSON)
public class TaskService {

	
	@Inject 
	EntityManager em;
	
	@Inject
	WorkQueue queue;
	
	@GET
	public List<Task> findAllTasks(){
		return em.createQuery("SELECT t FROM Task t").getResultList();	
	}
	
	@GET
	@Path("/{id}")
	public Response findTask(@PathParam("id") Integer id){
		Task task = em.find(Task.class	, id);	
		if (task == null) throw new NotFoundException();
		
		return Response.accepted().entity(task).build();
	}
	
	@POST
	public Response requestTask(@Context UriInfo uriinfo) throws Exception{
		
		Integer id = queue.submitTask(new Supplier<String>() {
			
			@Override
			public String get() {
				try {
					Thread.sleep((int)(50000 * Math.random()) + 2000);
					return new String("" + Math.random() * 10000000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException("Supplier cannot do it!");
				}
			}
		});
		return Response.accepted().header("Location", uriinfo.getBaseUriBuilder().path("/queue/" + id).build()).build();
	}
}
