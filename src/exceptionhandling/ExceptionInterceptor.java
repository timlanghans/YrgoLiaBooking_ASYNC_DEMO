package exceptionhandling;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolationException;

import db.DBException;


/**
 * To achieve a unified exception handling. As seen in an  article from JAXCenter.de
 * 
 * @see {@link = "https://jaxenter.de/rest-api-exception-handling-integration-von-beanvalidation-24560"}
 * @author Tim Langhans
 *
 */
public class ExceptionInterceptor {

	@Inject
	Logger logger ;
	
	@AroundInvoke
	public Object checkForException(InvocationContext ctx) throws Exception{
		Object proceedResponse;
		try{
			proceedResponse = ctx.proceed();
			return proceedResponse;
		} catch (Exception e){ 
			logger.log(Level.SEVERE, e.getMessage(), e);
			
			Object errorResponse;
			if (e instanceof ApiException){
				errorResponse = ((ApiException) e).getHttpResponse();
			} else if (e instanceof DBException){
				errorResponse = new ApiException( (DBException)e ).getHttpResponse();
			} else if (e instanceof ConstraintViolationException){
				throw (ConstraintViolationException)e;
			} else {
				logger.log(Level.SEVERE, "UNHANDLED EXCEPTION: " + e.getMessage(),e);
				errorResponse = new ApiException(e).getHttpResponse();
			}
			return errorResponse;
		}
	}
}
