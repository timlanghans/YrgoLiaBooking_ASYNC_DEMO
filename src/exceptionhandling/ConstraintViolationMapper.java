package exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException>{


	@Override
	public Response toResponse(ConstraintViolationException exception) {
		
		Set<ConstraintViolation<?>> constViolations = exception.getConstraintViolations();
        List<ConstraintViolationEntry> errorList = new ArrayList<>();
        for (ConstraintViolation<?> constraintViolation : constViolations) {
            errorList.add(new ConstraintViolationEntry(constraintViolation));
        }
        GenericEntity<List<ConstraintViolationEntry>> entity =
                new GenericEntity<List<ConstraintViolationEntry>>(errorList) {};
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(entity).build();
	}

}
