package exceptionhandling;

import java.util.Iterator;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConstraintViolationEntry {
 
    private String fieldName;
    private String wrongValue;
    private String errorMessage;
 
    public ConstraintViolationEntry() {}
 
    public ConstraintViolationEntry(ConstraintViolation violation) {
        Iterator<Path.Node> iterator = violation.getPropertyPath().iterator();
        Path.Node currentNode = iterator.next();
        String invalidValue = "";
        
        if (violation.getInvalidValue() != null) {
            invalidValue = violation.getInvalidValue().toString();
        }
        this.fieldName = currentNode.getName();
        this.wrongValue = invalidValue; 
        this.errorMessage = violation.getMessage();
    }
 
    public String getFieldName() {
        return fieldName;
    }
 
    public String getWrongValue() {
        return wrongValue;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
 
}