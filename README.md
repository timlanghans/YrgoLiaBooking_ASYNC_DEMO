###JUST FOR DEMO!

#How to run:
This is an excerpt of the YrgoLiaBooking application which is to be run in 
Docker containers to be found in the private Docker-registry of the main project
by the time beeing. 

#Why I did this:
Wanted to learn more on async-processes in REST interface. Used AsyncResponse 
and CompletionCallback class from JAX-RS to make an endpoint async in 
VisitsService.class. Furthermore I implemented a POST/Queue/SeeOther solution
for long running tasks that are asynchronously processed. Here I used the new 
CompletableFuture classes and Suppliers from Java8. 